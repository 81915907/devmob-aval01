const assets = [
    "index.html",
    "css/sb-admin-2.css",
    "css/sb-admin-2.min.css"
]
const staticDevSB = "sb-admin.av1"

self.addEventListener("install", (installEvent)  => {
    installEvent.waitUntil(
        caches.open(staticDevSB).then(
            (cache) => cache.addAll(assets)));
 }
);

self.addEventListener("fetch", (fetchEvent)  => {
    console.log('fetch url capturada:', fetchEvent.request.url);
    fetchEvent.respondWith(
        caches.match(fetchEvent.request).then(
            (cachedResponse) => {
                if (cachedResponse) {
                    return cachedResponse;
                }
                return fetch(fetchEvent.request);

        }),
    );
});

